import axios from "axios";

const Api = axios.create({
    baseURL: 'https://puskesmasku-api.test/api'
})

export default Api
