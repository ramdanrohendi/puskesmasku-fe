import { createRouter, createWebHistory } from 'vue-router'
import { isTokenExpire, isRole } from '../stores/jwtControl'
import DefaultLayout from '@/layouts/DefaultLayout.vue'
import AuthLayout from '@/layouts/AuthLayout.vue'
import AdminLayout from '@/layouts/AdminLayout.vue'

import IndexView from '@/views/IndexView.vue'
import RiwayatView from '@/views/RiwayatView.vue'

import LoginView from '@/views/LoginView.vue'
import RegisterView from '@/views/RegisterView.vue'

import DashboardView from '@/views/admin/dashboard/DashboardView.vue'
import PasienView from '@/views/admin/pasien/PasienView.vue'
import MedicalView from '@/views/admin/pasien/medicalhistory/MedicalView.vue'
import FormMedicalView from '@/views/admin/pasien/medicalhistory/FormMedicalView.vue'
import BannerView from '@/views/admin/content/banner/BannerView.vue'
import ServiceView from '@/views/admin/content/service/ServiceView.vue'
import FormServiceView from '@/views/admin/content/service/FormServiceView.vue'
import ShowDoctorView from '@/views/admin/content/showdoctor/ShowDoctorView.vue'
import FormShowDoctorView from '@/views/admin/content/showdoctor/FormShowDoctorView.vue'
import ScheduleView from '@/views/admin/content/schedule/ScheduleView.vue'
import FormScheduleView from '@/views/admin/content/schedule/FormScheduleView.vue'
import LocationView from '@/views/admin/content/location/LocationView.vue'
import ContactView from '@/views/admin/content/contact/ContactView.vue'
import FormContactView from '@/views/admin/content/contact/FormContactView.vue'

import WebsiteSettingView from '@/views/admin/configuration/websitesetting/WebsiteSettingView.vue'
import UserView from '@/views/admin/configuration/user/UserView.vue'
import FormUserView from '@/views/admin/configuration/user/FormUserView.vue'

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: '/',
      component: DefaultLayout,
      children: [
        {
          path: '/',
          name: 'home',
          component: IndexView,
        },
        {
          path: '/riwayat',
          name: 'riwayat',
          component: RiwayatView,
          meta: {
            title: "Riwayat",
            middleware: ['auth']
          },
        },
      ]
    },
    {
      path: '/',
      component: AuthLayout,
      meta: {
        middleware: ['guest']
      },
      children: [
        {
          path: "/login",
          name: "login",
          component: LoginView,
          meta: {
            title: "Login",
          },
        },
        {
          path: "/register",
          name: "register",
          component: RegisterView,
          meta: {
            title: "Register",
          },
        },
      ]
    },
    {
      path: '/admin',
      component: AdminLayout,
      redirect: '/admin/dashboard',
      children: [
        {
          path: "/admin/dashboard",
          name: "dashboard-admin",
          component: DashboardView,
          meta: {
            title: "Dashboard",
            middleware: ['except-pasien']
          },
        },
        {
          path: "/admin/pasien",
          name: "pasien-admin",
          component: PasienView,
          meta: {
            title: "Pasien",
            middleware: ['except-pasien']
          },
        },
        {
          path: '/admin/pasien/:patient_id/medical-history',
          name: 'medical-history',
          component: MedicalView,
          meta: {
            title: "Medical History",
            middleware: ['except-pasien']
          },
        },
        {
          path: '/admin/pasien/:patient_id/medical-history/form',
          name: 'medical-history-form',
          component: FormMedicalView,
          meta: {
            title: "Medical History Form",
            middleware: ['except-pasien']
          },
        },
        {
          path: '/admin/pasien/:patient_id/medical-history/:id/edit',
          name: 'medical-history-edit',
          component: FormMedicalView,
          meta: {
            title: "Medical History Form",
            middleware: ['except-pasien']
          },
        },
        {
          path: "/admin/banner",
          name: "banner-admin",
          component: BannerView,
          meta: {
            title: "Banner",
            middleware: ['except-pasien']
          },
        },
        {
          path: "/admin/service",
          name: "service-admin",
          component: ServiceView,
          meta: {
            title: "Service",
            middleware: ['except-pasien']
          },
        },
        {
          path: "/admin/show-doctor",
          name: "show-doctor-admin",
          component: ShowDoctorView,
          meta: {
            title: "Show Doctor",
            middleware: ['except-pasien']
          },
        },
        {
          path: "/admin/schedule",
          name: "schedule-admin",
          component: ScheduleView,
          meta: {
            title: "Schedule",
            middleware: ['except-pasien']
          },
        },
        {
          path: "/admin/location",
          name: "location-admin",
          component: LocationView,
          meta: {
            title: "Location",
            middleware: ['except-pasien']
          },
        },
        {
          path: "/admin/contact",
          name: "contact-admin",
          component: ContactView,
          meta: {
            title: "Contact",
            middleware: ['except-pasien']
          },
        },
        {
          path: "/admin/website-setting",
          name: "website-setting-admin",
          component: WebsiteSettingView,
          meta: {
            title: "Website Setting",
            middleware: ['except-pasien']
          },
        },
        {
          path: "/admin/pengguna",
          name: "pengguna-admin",
          component: UserView,
          meta: {
            title: "Pengguna",
            middleware: ['except-pasien']
          },
        },
      ]
    },
  ]
})

router.beforeEach((toRoute,fromRoute,next) => {
  if (toRoute.meta.title) {
    let getAppName = window.document.title.split(" - ")[0];
    window.document.title = getAppName + ' - ' + toRoute.meta.title;
  }

  if (toRoute.meta) {
    if (toRoute.meta.middleware) {
      const middleware = toRoute.meta.middleware
      if (middleware.includes('guest')) {
        if (isTokenExpire()) {
          next()
        } else {
          next({name: 'home'})
        }
      } else if (middleware.includes('auth')) {
        if (isTokenExpire()) {
          next({name: 'login'})
        } else {
          next()
        }
      } else if (middleware.includes('dokter')) {
        if (isRole('dokter')) {
          next()
        } else {
          next({name: 'home'})
        }
      } else if (middleware.includes('admin')) {
        if (isRole('admin')) {
          next()
        } else {
          next({name: 'home'})
        }
      } else if (middleware.includes('except-pasien')) {
        if (isRole('admin') || isRole('doctor')) {
          next()
        } else {
          next({name: 'home'})
        }
      } else {
        next();
      }
    } else {
      next();
    }
  } else {
    next();
  }
})

export default router
