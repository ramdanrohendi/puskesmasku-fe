const ID_DATA_KEY = "data_login" as string;

const jwtParse = (token: string) => {
    return JSON.parse(atob(token.split('.')[1]))
}

export const isTokenExpire = (): boolean => {
    const token = getToken()
    
    if (token) {
        if (jwtParse(token).exp < Date.now() / 1000) {
            return true
        } else {
            return false
        }
    }

    return true
};

export const getDataJWT = (): string | null => {
    return window.localStorage.getItem(ID_DATA_KEY);
};

export const getToken = (): string | null => {
    const dataFromLocalStorage = getDataJWT()
    let userToken
    
    if (dataFromLocalStorage) {
        const dataUser = JSON.parse(dataFromLocalStorage);
        userToken = dataUser.token;
    }

    return userToken
};

export const getUser = (): object | null => {
    const dataFromLocalStorage = getDataJWT()
    let userData
    
    if (dataFromLocalStorage) {
        const dataUser = JSON.parse(dataFromLocalStorage);
        userData = dataUser.user;
    }

    return userData
};

export const isRole = (role: string): boolean => {
    const user = getUser()

    if (user) {
        if (user.role == role) {
            return true
        }
    }

    return false
};

export const saveDataJWT = (data: string): void => {
    window.localStorage.setItem(ID_DATA_KEY, data);
};

export const destroyDataJWT = (): void => {
    window.localStorage.removeItem(ID_DATA_KEY);
};

export default { isTokenExpire, getDataJWT, getToken, getUser, saveDataJWT, destroyDataJWT };
